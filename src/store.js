import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default new Vuex.Store({
  state: {
     searchResult: []
  },
  actions: {
    loadSearchResult ({ commit }, query) {
      Vue.axios.get('https://sgs.sputnik.ru/?type=regions&format=json&query=' + query)
        .then((response) => {
          let entire = response.data.toString()
          let body = entire.substr(5, entire.length - 6)
          commit('SET_SEARCH_RESULT', body)
        })
    }
  },
  mutations: {
    SET_SEARCH_RESULT (state, searchResult) {
      state.searchResult = searchResult
    }
  },
  getters:   {
    getSearchResult: (state) => {
      return state.searchResult
    }
  }
})
